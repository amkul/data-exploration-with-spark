# Load the data and get a quick sense
filePath = 'datasets/NYPD_7_Major_Felony_Incidents.csv'
data=sc.textFile(filePath)

data.take(10)
data_without_header = data.filter(lambda x: x != header)
data_without_header.first()
data_without_header.map(lambda x: x.split(',')).take(10)

import csv
from io import StringIO
from collections import namedtuple

fields = header.replace(' ', '_').replace('/', '_').split(',')

print(fields)
Crime = namedtuple('Crime', fields, verbose=True)

def parse(row):
    reader = csv.reader(StringIO(row))
    row=next(reader)
    return Crime(*row)

crimes = data_without_header.map(parse)
crimes.first()
crimes.first().Offense
crimes.map(lambda x: x.Offense).countByValue()
crimes.map(lambda x: x.Occurrence_Year).countByValue()

crimesFiltered = crimes.filter(lambda x: not (x.Offense=='NA' or x.Occurrence_Year==''))\
        .filter(lambda x: int(x.Occurrence_Year)>=2006)
crimesFiltered.map(lambda x:x.Occurrence_Year).countByValue()

def extractCoords(location):
    location_lat = float(location[1:location.index(',')])
    location_lon = float(location[location.index(',')+1:-1])
    return (location_lat, location_lon)

crimesFiltered.map(lambda x: extractCoords(x.Location_1))\
    .reduce(lambda x,y: (min(x[0], y[0]), min(x[1], y[1])))

crimesFiltered.map(lambda x: extractCoords(x.Location_1))\
    .reduce(lambda x,y: (max(x[0], y[0]), max(x[1], y[1])))

crimeFinal = crimesFiltered.filter(lambda x: extractCoords(x.Location_1)[0] >= 40.477399 and \
                                  extractCoords(x.Location_1)[0] >= 40.477399 and \
                                  extractCoords(x.Location_1)[1] >= -74.25909 and \
                                  extractCoords(x.Location_1)[1] <= -73.700009)

# Trend By Year
crimeFinal.map(lambda x: x.Occurrence_Year).countByValue()

crimeFinal.filter(lambda x: x.Offense=='BURGLARY') \
            .map(lambda x: x.Occurrence_Year) \
            .countByValue()

import gmplot
gmap = gmplot.GoogleMapPlotter(37.428, -122.145, 16).from_geocode('New York City')

b_lats = crimeFinal.filter(lambda x: x.Offense=='BURGLARY' and x.Occurrence_Year=='2015') \
                    .map(lambda x: extractCoords(x.Location_1)[0]) \
                    .collect()

b_lons = crimeFinal.filter(lambda x: x.Offense=='BURGLARY' and x.Occurrence_Year=='2015') \
                    .map(lambda x: extractCoords(x.Location_1)[0]) \
                    .collect()


gmap.scatter(b_lats, b_lons, '#4440C8', size=40, marker=False)

gmap.draw('mymap.html')
